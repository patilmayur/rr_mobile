# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups



create table patilm.roles (
  role_id                   number(19) not null,
  modified_by               number(19),
  modified_dt               timestamp,
  active_yn                 varchar2(255),
  role_short_description    varchar2(255),
  role_long_description     varchar2(255),
  constraint pk_roles primary key (role_id))
;

create table patilm.user_details (
  userid                    number(19) not null,
  username                  varchar2(255),
  password                  varchar2(255),
  constraint pk_user_details primary key (userid))
;

create sequence role_id_seq;

create sequence role_id_seq;




# --- !Downs

drop table patilm.person cascade constraints purge;

drop table patilm.roles cascade constraints purge;

drop table patilm.user_details cascade constraints purge;

drop sequence role_id_seq;

drop sequence role_id_seq;

