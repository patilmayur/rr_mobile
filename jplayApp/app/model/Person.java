package model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name="person",schema=Person.SCHEMA) //, ,
public class Person  extends Model
{
	
	/**
	 * Recommended by interface serializable.
	 */
	private static final long serialVersionUID = 1;
	
	
	public static final String SCHEMA="AIUM20_DBA";
	
	
	/**
	 * The person's ID.
	 */
	@Id
    @SequenceGenerator(schema=Roles.SCHEMA,name="gen", sequenceName="role_id_seq",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="gen")
	@Column(name="PR_PERSON_ID")
	private int personId;
	
	/**
	 * The person's role ID.
	 */
	
	@Column(name="ROLE_ID")
	private int roleId;
	
	/**
	 * DOB
	 */
	
	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;
	
	/**
	 * Academic Degrees
	 */

	@Column(name="ACADEMIC_DEGREES")
	private String academicDegrees;
	
	/**
	 * The person's first name.
	 */
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	/**
	 * The person's middle name.
	 */
	
	@Column(name="MIDDLE_MNAME")
	private String middleName;
	/**
	 * The person's last name.
	 */
	
	@Column(name="LAST_NAME")
	private String lastName;
	/**
	 * The person's suffix.
	 */
	
	@Column(name="SUFFIX")
	private String suffix;
	/**
	 * Email Address
	 */
	
	@Column(name="EMAIL")
	private String emailAddress;
	/**
	 * Affiliation
	 */
	
	@Column(name="AFFILIATION")
	private String affiliation;
	/**
	 * Address1
	 */
	
	@Column(name="ADDRESS1")
	private String address1;
	
	/**
	 * Address2
	 */
	
	@Column(name="ADDRESS2")
	private String address2;
	
	/**
	 * City
	 */
	
	@Column(name="CITY")
	private String city;
	
	/**
	 * State
	 */
	
	@Column(name="STATE")
	private String state;
	
	/**
	 * Zip Code
	 */
	
	@Column(name="ZIP_CODE")
	private String zipCode;
	
	/**
	 * Country Code
	 */
	
	@Column(name="COUNTRY_CODE")
	private String countryCode;
	
	/**
	 * Phone Number
	 */
	
	@Column(name="PHONE_NO")
	private String phoneNumber;
	
	/**
	 * Alternate Phone
	 */
	
	@Column(name="ALTERNATE_PHONE")
	private String alternatePhone;
	
	/**
	 * Fax Number
	 */
	
	@Column(name="FAX_NO")
	private String faxNumber;
	
	/**
	 * Additional Info
	 */
	
	@Column(name="ADDITIONAL_INFO")
	private String additionalInfo;
	
	/**
	 * The person's username.
	 */
	
	@Column(name="USER_NAME")
	private String username;
	
	/**
	 * Password
	 */
	
	@Column(name="PASSWORD")
	private String password;
	
	/**
	 * Active Yn
	 */
	
	@Column(name="ACTIVE_YN")
	private String activeYn;
	
	/**
	 * Inactive Date
	 */
	
	@Column(name="INACTIVE_DT")
	private Date inactiveDate;
	
	/**
	 * Inactive Reason
	 */
	
	@Column(name="INACTIVE_REASON")
	private String inactiveReason;
	
	/**
	 * Updated By
	 */
	
	@Column(name="UPDATED_BY")
	private int updatedBy;
	
	/**
	 * Updated Date
	 */
	
	@Column(name="UPDATED_DT")
	private Date updatedDate;
	
	/**
	 * Lock Yn
	 */
	
	@Column(name="LOCK_YN")
	private String lockYn;
	
	/**
	 * Created By
	 */
	
	@Column(name="CREATED_BY")
	private int createdBy;
	
	/**
	 * Created Date
	 */
	
	@Column(name="CREATED_DT")
	private Date createdDate;
	
	/**
	 * The person's title.
	 */
	
	@Column(name="TITLE")
	private String title;
	
	/**
	 * Address3
	 */
	
	@Column(name="ADDRESS3")
	private String address3;
	
	/**
	 * Alternate Email
	 */
	
	@Column(name="ALTERNATE_EMAIL")
	private String alternateEmail;
	
	/**
	 * Job TItle
	 */
	
	@Column(name="JOB_TITLE")
	private String jobTitle;
	
	/**
	 * Discipine Code
	 */
	
	@Column(name="DISCIPLINE_CODE")
	private String disciplineCode;
	
	/**
	 * Prof Body Member Status
	 */
	
	@Column(name="PROF_BODY_MEMBER_STATUS")
	private String profBodyMemberStatus;
	
	/**
	 * Prof Body Member Number
	 */
	
	@Column(name="PROF_BODY_MEMBER_NO")
	private String profBodyMemberNumber;
	
	/**
	 * Author Yn
	 */
	
	@Column(name="AUTHOR_YN")
	private String authorYn;
	
	/**
	 * Local Office Address
	 */
	
	@Column(name="LOCAL_OFFICE_ADDRESS")
	private String localOfficeAddress;
	
	/**
	 * Show All Yn
	 */
	
	@Column(name="SHOW_ALL_YN")
	private String showAllYn;
	
	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}
	/**
	 * @param suffix the suffix to set
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the updatedBy
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}
	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the profBodyMemberNumber
	 */
	public String getProfBodyMemberNumber() {
		return profBodyMemberNumber;
	}
	/**
	 * @param profBodyMemberNumber the profBodyMemberNumber to set
	 */
	public void setProfBodyMemberNumber(String profBodyMemberNumber) {
		this.profBodyMemberNumber = profBodyMemberNumber;
	}
	/**
	 * @return the profBodyMemberStatus
	 */
	public String getProfBodyMemberStatus() {
		return profBodyMemberStatus;
	}
	/**
	 * @param profBodyMemberStatus the profBodyMemberStatus to set
	 */
	public void setProfBodyMemberStatus(String profBodyMemberStatus) {
		this.profBodyMemberStatus = profBodyMemberStatus;
	}

	/**
	 * @return the roleId
	 */
	public int getRoleId() {
		return roleId;
	}
	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	/**
	 * @return the showAllYn
	 */
	public String getShowAllYn() {
		return showAllYn;
	}
	/**
	 * @param showAllYn the showAllYn to set
	 */
	public void setShowAllYn(String showAllYn) {
		this.showAllYn = showAllYn;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the academicDegrees
	 */
	public String getAcademicDegrees() {
		return academicDegrees;
	}
	/**
	 * @param academicDegrees the academicDegrees to set
	 */
	public void setAcademicDegrees(String academicDegrees) {
		this.academicDegrees = academicDegrees;
	}
	/**
	 * @return the activeYn
	 */
	public String getActiveYn() {
		return activeYn;
	}
	/**
	 * @param activeYn the activeYn to set
	 */
	public void setActiveYn(String activeYn) {
		this.activeYn = activeYn;
	}
	/**
	 * @return the additionalInfo
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	/**
	 * @param additionalInfo the additionalInfo to set
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	/**
	 * @return the address3
	 */
	public String getAddress3() {
		return address3;
	}
	/**
	 * @param address3 the address3 to set
	 */
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	/**
	 * @return the affiliation
	 */
	public String getAffiliation() {
		return affiliation;
	}
	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	/**
	 * @return the alternateEmail
	 */
	public String getAlternateEmail() {
		return alternateEmail;
	}
	/**
	 * @param alternateEmail the alternateEmail to set
	 */
	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}
	/**
	 * @return the alternatePhone
	 */
	public String getAlternatePhone() {
		return alternatePhone;
	}
	/**
	 * @param alternatePhone the alternatePhone to set
	 */
	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}
	/**
	 * @return the authorYn
	 */
	public String getAuthorYn() {
		return authorYn;
	}
	/**
	 * @param authorYn the authorYn to set
	 */
	public void setAuthorYn(String authorYn) {
		this.authorYn = authorYn;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the createdBy
	 */
	public int getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	/**
	 * @return the disciplineCode
	 */
	public String getDisciplineCode() {
		return disciplineCode;
	}
	/**
	 * @param disciplineCode the disciplineCode to set
	 */
	public void setDisciplineCode(String disciplineCode) {
		this.disciplineCode = disciplineCode;
	}
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}
	/**
	 * @param faxNumber the faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the inactiveDate
	 */
	public Date getInactiveDate() {
		return inactiveDate;
	}
	/**
	 * @param inactiveDate the inactiveDate to set
	 */
	public void setInactiveDate(Date inactiveDate) {
		this.inactiveDate = inactiveDate;
	}
	/**
	 * @return the inactiveReason
	 */
	public String getInactiveReason() {
		return inactiveReason;
	}
	/**
	 * @param inactiveReason the inactiveReason to set
	 */
	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}
	/**
	 * @return the jobTitle
	 */
	public String getJobTitle() {
		return jobTitle;
	}
	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the localOfficeAddress
	 */
	public String getLocalOfficeAddress() {
		return localOfficeAddress;
	}
	/**
	 * @param localOfficeAddress the localOfficeAddress to set
	 */
	public void setLocalOfficeAddress(String localOfficeAddress) {
		this.localOfficeAddress = localOfficeAddress;
	}
	/**
	 * @return the lockYn
	 */
	public String getLockYn() {
		return lockYn;
	}
	/**
	 * @param lockYn the lockYn to set
	 */
	public void setLockYn(String lockYn) {
		this.lockYn = lockYn;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the personId
	 */
	public int getPersonId() {
		return personId;
	}
	/**
	 * @param personId the personId to set
	 */
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
 /**
  * Returns a string representation of this value object.
  *
  * This method is suitable for logging and debugging because it reveals
  * the state of all member variables.
  *
  * @return The string representation.
  */
 public String toString()
 {
     StringBuffer buffer;

     buffer = new StringBuffer();

     buffer.append( "Title: " );
     buffer.append( title );
     buffer.append( ", First Name: " );
     buffer.append( firstName );
     buffer.append( ", Middle Name: " );
     buffer.append( middleName );
     buffer.append( ", Last Name: " );
     buffer.append( lastName );
     buffer.append( ", Suffix: " );
     buffer.append( suffix );
     buffer.append( ", Username: " );
     buffer.append( username );
     buffer.append( ", Person ID: " );
     buffer.append( personId );
     buffer.append( ", Role ID: " );
     buffer.append( roleId );

     return buffer.toString();
 }
}

