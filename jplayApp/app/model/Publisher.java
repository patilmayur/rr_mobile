package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import play.db.ebean.Model;

@Entity
@Table(name="publisher",schema="RRCORE")// 
public class Publisher extends Model{
	
	
	/**
	 * 
	 */
	public static final long serialVersionUID = 1L;
	
	
	/**
	 * The publisher's ID (surrogate primary key).
	 */
	
	@Id
    @SequenceGenerator(schema=Roles.SCHEMA,name="gen", sequenceName="role_id_seq",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="gen")
	@Column(name ="PUBLISHER_ID")
	private int publisherId;
	
	/**
	 * The publisher's software major version number.
	 */
	@Column(name ="MAJOR_VERSION_NUMBER")
	private int majorVersion;
	
    /**
     * The publisher's acronym.
     */
	@Column(name ="ACRONYM")
	private String acronym;
	
	/**
	 * Whether or not the publisher's data is corrupt.
	 */
	@Column(name ="DATA_CORRUPT_YN")
	private String isDataCorrupt;
	

    /**
     * The publisher's web context root.
     */
	@Column(name ="WEB_CONTEXT_ROOT")
	private String webContextRoot;
    
    /**
     * The publisher's full name.
     */
	@Column(name ="FULL_NAME")
    private String fullName;
    
    /**
     * The publisher's database schema.
     */
	@Column(name ="DATABASE_SCHEMA")
	private String databaseSchema;
    
    /**
     * The publisher's service name.
     */
	@Column(name ="SERVICE_NAME")
	private String serviceName;
    
    /**
     * The publisher's URL.
     */
	@Column(name ="PUBLISHER_URL")
	private String publisherURL;    
    
    /*public Publisher( int publisherId, String acronym, String fullName, 
    				  String databaseSchema, String webContextRoot, 
    				  boolean isDataCorrupt, int majorVersion, 
    				  String serviceName, String publisherURL )
    {
    	this.publisherId = publisherId;
    	this.acronym = acronym;
    	this.fullName = fullName;
    	this.databaseSchema = databaseSchema;
    	this.webContextRoot = webContextRoot;
    	this.isDataCorrupt = isDataCorrupt;
    	this.majorVersion = majorVersion;
    	this.serviceName = serviceName;
    	this.publisherURL = publisherURL;
    }*/
    
    /**
     * Returns the publisher's acronym.
     * 
     * @return The acronym.
     */
    public String getAcronym()
    {
    	return acronym;
    }
    
    /**
     * Returns the publisher's database schema.
     * 
     * @return The database schema.
     */
    public String getDatabaseSchema()
    {
    	return databaseSchema;
    }
    
    /**
     * Returns the publisher's full name.
     * 
     * @return The full name.
     */
    public String getFullName()
    {
    	return fullName;
    }
    
    /**
     * Returns the publisher's major version number.
     *
     * @return The major version.
     */
    public int getMajorVersion()
    {
    	return majorVersion;
    }
    
    /**
     * Returns the publisher's ID (surrogate primary key).
     * 
     * @return The publisher ID.
     */
    public int getPublisherId()
    {
    	return publisherId;
    }
    
    /**
     * Returns the publisher's service name.
     * 
     * @return The service name.
     */
    public String getServiceName()
    {
    	return serviceName;
    }
    
    /**
     * Returns the publisher's web context root.
     * 
     * @return The web context root.
     */
    public String getWebContextRoot()
    {
    	return webContextRoot;
    }
    
    /**
     * Returns true if the publisher's data is corrupt,
     * false otherwise.
     * 
     * @return true if the publisher's data is corrupt.
     */
    public String isDataCorrupt()
    {
    	return isDataCorrupt;
    }
    
    /**
     * Returns the publisher's URL.
     * 
     * @return The publisher's URL
     */
    public String getPublisherURL()
    {
    	return publisherURL;
    }    
    
    /**
     * Returns a string representation of this value object.
     *
     * This method is suitable for logging and debugging because it reveals
     * the state of all member variables.
     *
     * @return The string representation.
     */
    public String toString()
    {
        StringBuffer buffer;

        buffer = new StringBuffer();

        buffer.append( "Publisher ID: " );
        buffer.append( publisherId );
        buffer.append( ", Acronym: " );
        buffer.append( acronym );
        buffer.append( ", Full Name: " );
        buffer.append( fullName );
        buffer.append( ", Database Schema: " );
        buffer.append( databaseSchema );   
        buffer.append( ", Web Context Root: " );
        buffer.append( webContextRoot );
        buffer.append( ", IsDataCorrupt: " );
        buffer.append( isDataCorrupt );
        buffer.append( ", Major Version: " );
        buffer.append( majorVersion );
        buffer.append( ", Service Name: " );
        buffer.append( serviceName );

        return buffer.toString();
    }

}
