package model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.db.ebean.Model;

/**
 *Class that encapsulates all the Holds data which can be used to populate the inbox and activebox of all the roles
 *@author name
 *@version 0
 */

@Entity
@Table(name="INBOX_ACTIVEBOX_VIEWER" )//, schema=Roles.SCHEMA //DEMO_DBA
public class InboxActiveBoxViewer extends Model
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//public static Log logger = StaticLogger.getLogger();
	
	/**
     *TaskId
     */
	@Id
	@Column(name="TASK_ID")
    private long taskId;
	

    /**
     *Role Id
     */
	@Column(name="ROLE_ID")
    private long roleId;

    /**
     *Inbox yn
     */
	@Column(name="IN_BOX_YN")
    private String inbox_yn;

	 /**
     *Sets the value of TaskId
     *@param rrTaskId TaskId
     *@return void
     */
    public void setTaskId(long rrTaskId)
    {
        taskId = rrTaskId;
    }

    /**
     *Gets the TaskId
     *@return taskId
     */
    public long getTaskId()
    {
        return taskId;
    }
   
    /**
     *Sets the value of Role Id
     *@param rrRoleId Role Id
     *@return void
     */
    public void setRoleId(long rrRoleId)
    {
        roleId = rrRoleId;
    }

    /**
     *Gets the Role Id
     *@return roleId
     */
    public long getRoleId()
    {
        return roleId;
    }

    /**
     *Sets the value of Inbox yn
     *@param rrInbox_yn Inbox yn
     *@return void
     */
    public void setInbox_yn(String rrInbox_yn)
    {
        /*int len = (rrInbox_yn.length() > 1) ? 1 : rrInbox_yn.length();
        rrInbox_yn = RRUtil.validateString(rrInbox_yn.substring(0, len), validateText);*/
        inbox_yn = rrInbox_yn;
    }

    /**
     *Gets the Inbox yn
     *@return inbox_yn
     */
    public String getInbox_yn()
    {
        return inbox_yn;
    }


}
