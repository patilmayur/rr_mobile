package model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import play.db.ebean.Model;

/**
 *Class that encapsulates all the Journal wise role information
 *@author P. Aravind Kumar
 *@version 1.0
 */

@Entity
@Table(name="PR_JOURNAL_ROLE_INFO",schema=Roles.SCHEMA )//DEMO_DBA
public class PersonJournalRoleInformation extends Model
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//public static Log logger = StaticLogger.getLogger();
	
	@EmbeddedId
	private PersonJournalRoleInformationPK primarykey;
	

	/**
	 *Journal code
	 */
	@Column(name="JOURNAL_CODE")
	private String journalCode;

	/**
	 *Section code
	 */
	@Column(name="SECTION_CODE")
	private String sectionCode;

	/**
	 *Role identity number
	 */
	@Column(name="ROLE_ID")
	private long roleId;
	
	/**
	 *Preferred MS Type code
	 */
	@Column(name="PREFERRED_MS_TYPE")
	private String prefMSTypeCode;
	
	@Column(name="MODIFIED_BY")
	private Long modifiedBy;
	
	@Column(name="MODIFIED_DT")
    private String modifiedDate;

	//Added for BUG 17457
      /**
       * Contact to staff
       * */	
	@Column(name="DISPLAY_CONTACT_YN")
	private String displayContactYN;
	
	
	/**
	 *Gets the next serial no from table
	 *@return long
	 */
	/*public long getNextSerialNo() throws SQLException
	{
		try{
		String[] slno=fetchSingle("select max(SL_NO) from  " + schemaName + ".PR_JOURNAL_ROLE_INFO where PR_PERSON_ID="+getPrPersonId());
		return (slno==null)?0l:RRUtil.stringToLong(slno[0])+1;
		}catch(Exception e){ throw new SQLException("Database Error while getting last Serial Number");}
	}*/

	/**
	 *Sets the value of Journal code
	 *@param rrJournalCode Journal code
	 *@return void
	 */
	public void setJournalCode(String rrJournalCode)
	{
		/*int len=(rrJournalCode.length()>10)?10:rrJournalCode.length();
		rrJournalCode=RRUtil.validateString(rrJournalCode.substring(0,len),validateText);*/
		journalCode = rrJournalCode;
	}

	/**
	 *Gets the Journal code
	 *@return journalCode
	 */
	public String getJournalCode()
	{
		 return journalCode;
	}

	/**
	 *Sets the value of Section code
	 *@param rrSectionCode Section code
	 *@return void
	 */
	public void setSectionCode(String rrSectionCode)
	{
		/*int len=(rrSectionCode.length()>10)?10:rrSectionCode.length();
		rrSectionCode=RRUtil.validateString(rrSectionCode.substring(0,len),validateText);*/
		sectionCode = rrSectionCode;
	}

	/**
	 *Gets the Section code
	 *@return sectionCode
	 */
	public String getSectionCode()
	{
		 return sectionCode;
	}

	/**
	 *Sets the value of Role identity number
	 *@param rrRoleId Role identity number
	 *@return void
	 */
	public void setRoleId(long rrRoleId)
	{
		roleId = rrRoleId;
	}

	/**
	 *Gets the Role identity number
	 *@return roleId
	 */
	public long getRoleId()
	{
		 return roleId;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
     * Get Contact to staff
     *
     * */

	public String getDisplayContactYN() {
		return displayContactYN;
	}

	public void setDisplayContactYN(String displayContactYN) {
		this.displayContactYN = displayContactYN;
	}

}