package model;


import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import org.h2.engine.Session;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.Query;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.avaje.ebean.annotation.Sql;


@Entity
@Sql
public class GetManuscriptDetails {
	
	
	  private String ms_no;
	  private String ms_version_no;
	  private String	TYPE_DESCRIPTION;
	  private String status;
	  private Date 	due_dt;
	  private int 	moderator_task_id;
	  private Date   warning_dt;
	  private Date 	assigned_dt;
	  private String journal_code;
	  private String last_name;
	  private String first_name;
	  private String short_description;
	  private String title;
	  private Date version_recieved_dt;
	  private String au_last_name;
	  private String moderator_id;
	  private String affiliation;
	  private String section;
	  private String short_title;
	  private String PAPER_INVITED_YN;
	  private String COPY_RIGHT_FORM_YN;
	  private String p_last_name;
	  private String p_first_name ;
	  private String uTitle ;
	  private String uShTitle ;
	  private String uauthor ;
	  private String uSection ;
	  private String uMsname ;
	  private String review_only_yn;
	  private String privateation_yn;
	  private String transfer_yn;
	  private String sortTask;
	  private String number;
	  private String publication_yn;
	  
	  
	  
	
	
	  public String getMs_no() {
		return ms_no;
	}



	public void setMs_no(String ms_no) {
		this.ms_no = ms_no;
	}



	public String getMs_version_no() {
		return ms_version_no;
	}



	public void setMs_version_no(String ms_version_no) {
		this.ms_version_no = ms_version_no;
	}



	public String getTYPE_DESCRIPTION() {
		return TYPE_DESCRIPTION;
	}



	public void setTYPE_DESCRIPTION(String tYPE_DESCRIPTION) {
		TYPE_DESCRIPTION = tYPE_DESCRIPTION;
	}



	public String getStatus() {
		return status;
	}



	public void setStatus(String status) {
		this.status = status;
	}



	public Date getDue_dt() {
		return due_dt;
	}



	public void setDue_dt(Date due_dt) {
		this.due_dt = due_dt;
	}



	public int getModerator_task_id() {
		return moderator_task_id;
	}



	public void setModerator_task_id(int moderator_task_id) {
		this.moderator_task_id = moderator_task_id;
	}



	public Date getWarning_dt() {
		return warning_dt;
	}



	public void setWarning_dt(Date warning_dt) {
		this.warning_dt = warning_dt;
	}



	public Date getAssigned_dt() {
		return assigned_dt;
	}



	public void setAssigned_dt(Date assigned_dt) {
		this.assigned_dt = assigned_dt;
	}



	public String getJournal_code() {
		return journal_code;
	}



	public void setJournal_code(String journal_code) {
		this.journal_code = journal_code;
	}



	public String getLast_name() {
		return last_name;
	}



	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}



	public String getFirst_name() {
		return first_name;
	}



	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}



	public String getShort_description() {
		return short_description;
	}



	public void setShort_description(String short_description) {
		this.short_description = short_description;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public Date getVersion_recieved_dt() {
		return version_recieved_dt;
	}



	public void setVersion_recieved_dt(Date version_recieved_dt) {
		this.version_recieved_dt = version_recieved_dt;
	}



	public String getAu_last_name() {
		return au_last_name;
	}



	public void setAu_last_name(String au_last_name) {
		this.au_last_name = au_last_name;
	}



	public String getModerator_id() {
		return moderator_id;
	}



	public void setModerator_id(String moderator_id) {
		this.moderator_id = moderator_id;
	}



	public String getAffiliation() {
		return affiliation;
	}



	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}



	public String getSection() {
		return section;
	}



	public void setSection(String section) {
		this.section = section;
	}



	public String getShort_title() {
		return short_title;
	}



	public void setShort_title(String short_title) {
		this.short_title = short_title;
	}



	public String getPAPER_INVITED_YN() {
		return PAPER_INVITED_YN;
	}



	public void setPAPER_INVITED_YN(String pAPER_INVITED_YN) {
		PAPER_INVITED_YN = pAPER_INVITED_YN;
	}



	public String getCOPY_RIGHT_FORM_YN() {
		return COPY_RIGHT_FORM_YN;
	}



	public void setCOPY_RIGHT_FORM_YN(String cOPY_RIGHT_FORM_YN) {
		COPY_RIGHT_FORM_YN = cOPY_RIGHT_FORM_YN;
	}



	public String getP_last_name() {
		return p_last_name;
	}



	public void setP_last_name(String p_last_name) {
		this.p_last_name = p_last_name;
	}



	public String getP_first_name() {
		return p_first_name;
	}



	public void setP_first_name(String p_first_name) {
		this.p_first_name = p_first_name;
	}



	public String getuTitle() {
		return uTitle;
	}



	public void setuTitle(String uTitle) {
		this.uTitle = uTitle;
	}



	public String getuShTitle() {
		return uShTitle;
	}



	public void setuShTitle(String uShTitle) {
		this.uShTitle = uShTitle;
	}



	public String getUauthor() {
		return uauthor;
	}



	public void setUauthor(String uauthor) {
		this.uauthor = uauthor;
	}



	public String getuSection() {
		return uSection;
	}



	public void setuSection(String uSection) {
		this.uSection = uSection;
	}



	public String getuMsname() {
		return uMsname;
	}



	public void setuMsname(String uMsname) {
		this.uMsname = uMsname;
	}



	public String getReview_only_yn() {
		return review_only_yn;
	}



	public void setReview_only_yn(String review_only_yn) {
		this.review_only_yn = review_only_yn;
	}



	public String getPublication_yn() {
		return publication_yn;
	}



	public void setPublication_yn(String publication_yn) {
		this.publication_yn = publication_yn;
	}



	public String getTransfer_yn() {
		return transfer_yn;
	}



	public void setTransfer_yn(String transfer_yn) {
		this.transfer_yn = transfer_yn;
	}



	public String getSortTask() {
		return sortTask;
	}



	public void setSortTask(String sortTask) {
		this.sortTask = sortTask;
	}

	
	
	
public List<GetManuscriptDetails> getManudriptDetails(User user){	
	
	
	String query= "SELECT DISTINCT "
			+ "mps.ms_no,"
	  +" mps.ms_version_no,"
	  +" mt.TYPE_DESCRIPTION,"
	  +" msd.status,"
	  +" mps.due_dt,"
	  +" mps.moderator_task_id,"
	  +" mps.warning_dt,"
	  +" mps.assigned_dt,"
	  +" msd.journal_code,"
	  +" a.last_name ||', ' ||a.first_name au_fullname,"
	  +" upper(jt.short_description),"
	  +" msd.title,"
	  +" msd.version_recieved_dt,"
	  +" a.last_name,"
	  +" mps.moderator_id,"
	  +" 1 as no,"
	  +" a.affiliation,"
	  +" msd.section,"
	  +" msd.short_title,"
	  +" mspd.PAPER_INVITED_YN,"
	  +" mprd.COPY_RIGHT_FORM_YN,"
	  +" p.last_name ||', '||p.first_name p_fullname,"
	  +" UPPER(msd.title) UTitle ,"
	  +" UPPER(NVL(msd.short_title,' ')) UShTitle ,"
	  +" UPPER(a.last_name ||', '||a.first_name) Uauthor ,"
	  +" UPPER(msd.section) USection ,"
	  +" UPPER(p.last_name ||', '||p.first_name) Umsname ,"
	  +" mspd.review_only_yn,"
	  +" mspd.publication_yn,"
	  +" mst.transfer_yn,"
	  +" DECODE(mps.moderator_task_id,2 ,-1,0) SortTask "
	+" FROM "+user.getPubSchema()+".ms_submission_detail msd,"
	  +user.getPubSchema()+".author a,"
	  +user.getPubSchema()+".jl_tasks jt,"
	  +user.getPubSchema()+".person p,"
	  +user.getPubSchema()+".ms_supplimentary_details mspd,"
	  +user.getPubSchema()+".ms_transfer_details mst,"
	  +user.getPubSchema()+".ms_production_data mprd,"
	  +user.getPubSchema()+".ms_prescreen_details mps,"
	  +user.getPubSchema()+".pr_journal_role_info pjr,"
	  +user.getPubSchema()+".inbox_activebox_viewer ib ,"
	  +user.getPubSchema()+".MS_TYPE_MAST mt "
	+" WHERE msd.ms_no           = mps.ms_no"
	+" AND msd.ms_version_no     = mps.ms_version_no"
	+" AND mspd.ms_no            = msd.ms_no"
	+" AND mspd.ms_version_no    = msd.ms_version_no"
	+" AND mst.ms_no(+)          = msd.ms_no"
	+" AND mst.ms_version_no(+)  = msd.ms_version_no"
	+" AND mprd.ms_no            = msd.ms_no"
	+" AND mprd.ms_version_no    = msd.ms_version_no"
	+" AND msd.active_yn         = 'Y'"
	+" AND p.pr_person_id        = mps.moderator_id"
	+" AND p.role_id             = 1"
	+" AND mps.moderator_id      = "+user.getUserid()//5382"
	+" AND msd.author_id         = a.au_author_id"
	+" AND msd.journal_code      = jt.journal_code"
	+" AND mps.moderator_task_id = jt.task_id"
	+" AND msd.journal_code      = pjr.journal_code"
	+" AND pjr.pr_person_id      = 5382"
	+" AND ib.role_id            =p.role_id"
	+" AND msd.section           = 'General'"
	+" AND ib.task_id            =mps.moderator_task_id"
	+" AND in_box_yn             ='Y'"
	+" AND mt.journal_code       =msd.journal_code"
	+" AND mt.MS_TYPE_CODE       = msd.MS_TYPE"
	+" AND msd.status NOT       IN (6,11,13,26)"
	+" UNION ALL "//------------------------------------------------------------------------------
	+" SELECT DISTINCT mas.ms_no,"
	  +" mas.ms_version_no,"
	  +" mt.TYPE_DESCRIPTION,"
	  +" msd.status,"
	  +" mas.due_dt,"
	  +" mas.task_id,"
	  +" mas.warning_dt,"
	  +" mas.assigned_dt,"
	  +" msd.journal_code,"
	  +" a.last_name ||', ' ||a.first_name,"
	  +" upper(jt.short_description),"
	  +" msd.title,"
	  +" msd.version_recieved_dt,"
	  +" a.last_name,"
	  +" 152,2 as no,"
	  +" a.affiliation,"
	  +" msd.section,"
	  +" msd.short_title,"
	  +" mspd.PAPER_INVITED_YN,"
	  +" mprd.COPY_RIGHT_FORM_YN,"
	  +" DECODE(mas.assigned_person_id,-1,'-Unassigned-',p.last_name ||', ' ||p.first_name) ,"
	  +" UPPER(msd.title) UTitle ,"
	  +" UPPER(NVL(msd.short_title,' ')) UShTitle ,"
	  +" UPPER(a.last_name ||', ' ||a.first_name) Uauthor ,"
	  +" UPPER(msd.section) USection ,"
	  +" UPPER(DECODE(mas.assigned_person_id,-1,'-Unassigned-',p.last_name ||', ' ||p.first_name)) Umsname ,"
	  +" mspd.review_only_yn,"
	  +" mspd.publication_yn,"
	  +" mst.transfer_yn,"
	  +" DECODE(mas.task_id,2 ,-1,0) SortTask"
	+" FROM "+user.getPubSchema()+".ms_submission_detail msd,"
	  +user.getPubSchema()+".ms_transfer_details mst,"
	  +user.getPubSchema()+".ms_assigned_staff mas,"
	  +user.getPubSchema()+".ms_supplimentary_details mspd,"
	  +user.getPubSchema()+".ms_production_data mprd,"
	  +user.getPubSchema()+".author a,"
	  +user.getPubSchema()+".pr_journal_role_info pjr,"
	  +user.getPubSchema()+".inbox_activebox_viewer ib,"
	  +user.getPubSchema()+".jl_tasks jt ,"
	  +user.getPubSchema()+".MS_TYPE_MAST mt,"
	  +user.getPubSchema()+".PERSON p"
	+" WHERE msd.ms_no          = mas.ms_no"
	+" AND msd.ms_version_no    = mas.ms_version_no"
	+" AND mspd.ms_no           = msd.ms_no"
	+" AND mspd.ms_version_no   = msd.ms_version_no"
	+" AND mst.ms_no(+)         = msd.ms_no"
	+" AND mst.ms_version_no(+) = msd.ms_version_no"
	+" AND mprd.ms_no           = msd.ms_no"
	+" AND mprd.ms_version_no   = msd.ms_version_no"
	+" AND mas.role_id          = 1"
	+" AND p.pr_person_id(+)    =mas.assigned_person_id"
	+" AND msd.active_yn        = 'Y'"
	+" AND msd.author_id        = a.au_author_id"
	+" AND mas.task_id          = jt.task_id"
	+" AND msd.journal_code     = jt.journal_code"
	+" AND msd.journal_code     = pjr.journal_code"
	+" AND pjr.pr_person_id     = "+user.getUserid()//5382"
	+" AND ib.role_id           =mas.role_id"
	+" AND ib.task_id           =mas.task_id"
	+" AND in_box_yn            ='Y'"
	+" AND mt.journal_code      =msd.journal_code"
	+" AND mt.MS_TYPE_CODE      = msd.MS_TYPE"
	+" AND msd.status NOT      IN (6,11,13,26)"
	+" AND mas.task_id NOT     IN (141,148)"
	+" AND msd.section          = 'General'"
	+" ORDER BY SortTask DESC,"
	  +" 5 DESC";
	
	EbeanServer server = Ebean.getServer(user.getPubSchema().toLowerCase());
	
	
	RawSql rawSql =   
		    RawSqlBuilder  
		        .unparsed(query)  
		        //.columnMapping("r.value", "value")  
		        .columnMapping("mps.ms_no","ms_no")
	  .columnMapping("mps.ms_version_no","ms_version_no")
	  .columnMapping("mt.TYPE_DESCRIPTION","TYPE_DESCRIPTION")
	  .columnMapping("msd.status","status")
	  .columnMapping("mps.due_dt","due_dt")
	  .columnMapping("mps.moderator_task_id","moderator_task_id")
	  .columnMapping("mps.warning_dt","warning_dt")
	  .columnMapping("mps.assigned_dt","assigned_dt")
	  .columnMapping("msd.journal_code","journal_code")
	  .columnMapping("au_fullname","first_name")
	  //.columnMapping("first_name")
	  .columnMapping("upper(jt.short_description)","short_description")
	  .columnMapping("msd.title","title")
	  .columnMapping("msd.version_recieved_dt","version_recieved_dt")
	  .columnMapping("a.last_name","au_last_name")
	  .columnMapping("mps.moderator_id","moderator_id")
	  .columnMapping("no","number")
	  .columnMapping("a.affiliation","affiliation")
	  .columnMapping("msd.section","section")
	  .columnMapping("msd.short_title","short_title")
	  .columnMapping("mspd.PAPER_INVITED_YN","PAPER_INVITED_YN")
	  .columnMapping("mprd.COPY_RIGHT_FORM_YN","COPY_RIGHT_FORM_YN")
	  //.columnMapping("p_last_name")
	  .columnMapping("p_fullname","p_first_name")
	  .columnMapping("msd.title","uTitle")
	  .columnMapping("UShTitle","uShTitle")
	  .columnMapping("Uauthor","uauthor")
	  .columnMapping("USection","uSection")
	  .columnMapping("Umsname","uMsname")
	  .columnMapping("mspd.review_only_yn","review_only_yn")
	  .columnMapping("mspd.publication_yn","publication_yn")
	  .columnMapping("mst.transfer_yn","transfer_yn")
	  .columnMapping("SortTask","sortTask")
		        .create();   

		Query<GetManuscriptDetails> queryResult = server.find(GetManuscriptDetails.class);  
		queryResult.setRawSql(rawSql);
//		    .where().gt("amount", 10);   
		
		List<GetManuscriptDetails> list = queryResult.findList(); 

		
		
		/*getTYPE_DESCRIPTION();
		  getStatus();
		  getDue_dt();
		  getModerator_task_id();
		  getWarning_dt();
		  getFirst_name();   ///AuthorName
		  //first_name
		  getVersion_recieved_dt();
		  getAu_last_name();
		  getModerator_id();
		  getAffiliation();
		  getSection();
		  getShort_title();
		  getPAPER_INVITED_YN();
		  getCOPY_RIGHT_FORM_YN();
		  //p_last_name
		  getP_first_name();
		  getuShTitle();
		  getUauthor();
		  getuSection();
		  getuMsname();
		  getReview_only_yn();
		  getPublication_yn();
		  getTransfer_yn();*/
		
		return list;
		
	}



public String getPrivateation_yn() {
	return privateation_yn;
}



public void setPrivateation_yn(String privateation_yn) {
	this.privateation_yn = privateation_yn;
}



public String getNumber() {
	return number;
}



public void setNumber(String number) {
	this.number = number;
}

}
