package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import play.data.format.Formats.DateTime;
import play.data.validation.*;
import play.db.ebean.Model;

@Entity
@Table(name="roles" )//, schema=Roles.SCHEMA
public class Roles extends Model{
//public class Roles implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String SCHEMA="SLACK20_DBA"; // patilm
	
	@Id
    @SequenceGenerator(schema=Roles.SCHEMA,name="gen", sequenceName="role_id_seq",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="gen")
	public long roleId=0;

	@Column(name="modified_by") 
	public long modifiedBy=0;

	@DateTime(pattern = "dd/mm/yy")
	@Column(name="modified_dt")
	public java.util.Date modifiedDate=null;

	@Constraints.Required 
	public String active_yn=null;

	@Column(name="role_short_description")
	@Constraints.Required
	public String roleShortDescription=null;

	@Column(name="role_long_description")
	@Constraints.Required
	public String roleLongDescription=null;


	public long getRoleId() {
		return roleId;
	}


	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}


	public long getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public java.util.Date getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(java.util.Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public String getActive_yn() {
		return active_yn;
	}


	public void setActive_yn(String active_yn) {
		this.active_yn = active_yn;
	}


	public String getRoleShortDescription() {
		return roleShortDescription;
	}


	public void setRoleShortDescription(String roleShortDescription) {
		this.roleShortDescription = roleShortDescription;
	}


	public String getRoleLongDescription() {
		return roleLongDescription;
	}


	public void setRoleLongDescription(String roleLongDescription) {
		this.roleLongDescription = roleLongDescription;
	}
	
	
	
}