package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import play.db.ebean.Model;

@Entity
@Table(name="AUTHOR")//, schema=Roles.SCHEMA
public class Author extends Model{
	
	/**
	 * @Table(name="AUTHOR", schema=Roles.SCHEMA)
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @SequenceGenerator(schema=Roles.SCHEMA,name="gen", sequenceName="role_id_seq",allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="gen")
	@Column(name="AU_AUTHOR_ID")
	private int AuAuthorId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;
	
	@Column(name="ACADEMIC_DEGREES")
	private String academicDegrees;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="MIDDLE_NAME")
	private String middleName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="SUFFIX")
	private String suffix;
	
	@Column(name="EMAIL")
	private String emailAddress;
	
	@Column(name="AFFILIATION")
	private String affiliation;
	
	@Column(name="ADDRESS1")
	private String address1;
	
	@Column(name="ADDRESS2")
	private String address2;
	
	@Column(name="CITY")
	private String city;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="ZIP_CODE")
	private String zipCode;
	
	@Column(name="COUNTRY")
	private String countryCode;
	
	@Column(name="TELEPHONE_NO")
	private String phoneNumber;
	
	@Column(name="ALTERNATE_PHONE")
	private String alternatePhone;
	
	@Column(name="FAX_NO")
	private String faxNumber;
	
	@Column(name="ADDITIONAL_INFO")
	private String additionalInfo;
	
	@Column(name="USER_NAME")
	private String userName;
	
	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="ACTIVE_YN")
	private String activeYn;
	
	@Temporal(TemporalType.DATE)
	@Column(name="INACT_DT")
	private Date inactiveDate;
	
	@Column(name="INACTIVE_REASON")
	private String inactiveReason;
	
	@Column(name="LAST_MODIFIED_BY")
	private int updatedBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="LAST_MODIFIED_DATE")
	private Date updatedDate;
	
	@Column(name="LOCK_YN")
	private String lockYn;
	
	@Column(name="CREATED_BY")
	private int createdBy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DT")
	private Date createdDate;
	
	@Column(name="TITLE")
	private String title;
	
	@Column(name="ADDRESS3")
	private String address3;
	
	@Column(name="ALTERNATE_EMAIL")
	private String alternateEmail;
	
	@Column(name="JOB_TITLE")
	private String jobTitle;
	
	/*@Column(name="")
	private String disciplineCode;*/
	
	@Column(name="PROF_BODY_MEMBER_STATUS")
	private String profBodyMemberStatus;
	
	@Column(name="PROF_BODY_MEMBER_NO")
	private String profBodyMemberNumber;
	
	@Column(name="")
	private String authorYn;
	
	@Column(name="UPGRADED_AS_STAFF_YN")
	private String upgradeAsStaffYn;
	
	@Column(name="ORCID_ID")
	private String orcidId;
	
	@Column(name="ORCID_VALIDATED_DT")
	private Date   orcidValidatedDate;
	
	
	@Column(name="AUTHOR_BIO")
	private String authorBio;
	
	@Column(name="AUTHOR_WEBSITE")
	private String authorWebsite;
	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}
	/**
	 * @param suffix the suffix to set
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the updatedBy
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}
	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}
	/**
	 * @param zipCode the zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the profBodyMemberNumber
	 */
	public String getProfBodyMemberNumber() {
		return profBodyMemberNumber;
	}
	/**
	 * @param profBodyMemberNumber the profBodyMemberNumber to set
	 */
	public void setProfBodyMemberNumber(String profBodyMemberNumber) {
		this.profBodyMemberNumber = profBodyMemberNumber;
	}
	/**
	 * @return the profBodyMemberStatus
	 */
	public String getProfBodyMemberStatus() {
		return profBodyMemberStatus;
	}
	/**
	 * @param profBodyMemberStatus the profBodyMemberStatus to set
	 */
	public void setProfBodyMemberStatus(String profBodyMemberStatus) {
		this.profBodyMemberStatus = profBodyMemberStatus;
	}
	/**
	 * @return the prPersonId
	 */
	public int getAuPersonId() {
		return AuAuthorId;
	}
	/**
	 * @param prPersonId the prPersonId to set
	 */
	public void setAuAuthorId(int auAuthorId) {
		this.AuAuthorId = auAuthorId;
	}
	public String getUpgradeAsStaffYn() {
		return upgradeAsStaffYn;
	}
	public void setUpgradeAsStaffYn(String upgradeAsStaffYn) {
		this.upgradeAsStaffYn = upgradeAsStaffYn;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the academicDegrees
	 */
	public String getAcademicDegrees() {
		return academicDegrees;
	}
	/**
	 * @param academicDegrees the academicDegrees to set
	 */
	public void setAcademicDegrees(String academicDegrees) {
		this.academicDegrees = academicDegrees;
	}
	/**
	 * @return the activeYn
	 */
	public String getActiveYn() {
		return activeYn;
	}
	/**
	 * @param activeYn the activeYn to set
	 */
	public void setActiveYn(String activeYn) {
		this.activeYn = activeYn;
	}
	/**
	 * @return the additionalInfo
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	/**
	 * @param additionalInfo the additionalInfo to set
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	/**
	 * @return the address3
	 */
	public String getAddress3() {
		return address3;
	}
	/**
	 * @param address3 the address3 to set
	 */
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	/**
	 * @return the affiliation
	 */
	public String getAffiliation() {
		return affiliation;
	}
	/**
	 * @param affiliation the affiliation to set
	 */
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	/**
	 * @return the alternateEmail
	 */
	public String getAlternateEmail() {
		return alternateEmail;
	}
	/**
	 * @param alternateEmail the alternateEmail to set
	 */
	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}
	/**
	 * @return the alternatePhone
	 */
	public String getAlternatePhone() {
		return alternatePhone;
	}
	/**
	 * @param alternatePhone the alternatePhone to set
	 */
	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}
	/**
	 * @return the authorYn
	 */
	public String getAuthorYn() {
		return authorYn;
	}
	/**
	 * @param authorYn the authorYn to set
	 */
	public void setAuthorYn(String authorYn) {
		this.authorYn = authorYn;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the countryCode
	 */
	public String getCountryCode() {
		return countryCode;
	}
	/**
	 * @param countryCode the countryCode to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	/**
	 * @return the createdBy
	 */
	public int getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the dateOfBirth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	/**
	 * @return the disciplineCode
	 *//*
	public String getDisciplineCode() {
		return disciplineCode;
	}
	*//**
	 * @param disciplineCode the disciplineCode to set
	 *//*
	public void setDisciplineCode(String disciplineCode) {
		this.disciplineCode = disciplineCode;
	}*/
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	/**
	 * @return the faxNumber
	 */
	public String getFaxNumber() {
		return faxNumber;
	}
	/**
	 * @param faxNumber the faxNumber to set
	 */
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the inactiveDate
	 */
	public Date getInactiveDate() {
		return inactiveDate;
	}
	/**
	 * @param inactiveDate the inactiveDate to set
	 */
	public void setInactiveDate(Date inactiveDate) {
		this.inactiveDate = inactiveDate;
	}
	/**
	 * @return the inactiveReason
	 */
	public String getInactiveReason() {
		return inactiveReason;
	}
	/**
	 * @param inactiveReason the inactiveReason to set
	 */
	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}
	/**
	 * @return the jobTitle
	 */
	public String getJobTitle() {
		return jobTitle;
	}
	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the lockYn
	 */
	public String getLockYn() {
		return lockYn;
	}
	/**
	 * @param lockYn the lockYn to set
	 */
	public void setLockYn(String lockYn) {
		this.lockYn = lockYn;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	public String getOrcidId() {
		return orcidId;
	}
	
	public void setOrcidId(String newVal) {
		this.orcidId = newVal;
	}
	
	/**
	 * @return the orcid validated date
	 */
	public Date getOrcidValidatedDate() {
		return orcidValidatedDate;
	}
	/**
	 * @param newVal the new date to set
	 */
		public void setOrcidValidatedDate(Date newVal) {
			this.orcidValidatedDate = newVal;
		} 
	
	 	public String getAuthorWebsite() {
			return authorWebsite;
		}

		public void setAuthorWebsite(String authorWebsite) {
			this.authorWebsite = authorWebsite;
		}

	    public String getAuthorBio() {
			return authorBio;
		}
	    
	    public void setAuthorBio(String authorBio) {
			this.authorBio = authorBio;
		}
	

}
