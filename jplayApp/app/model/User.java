package model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import play.data.validation.*;
import play.db.ebean.Model;

//@Entity
//@Table(name="user_details",schema=User.SCHEMA)
public class User{
    
	public static final String SCHEMA="patilm";
	
//	@Id
//    @SequenceGenerator(schema=Roles.SCHEMA,name="gen", sequenceName="role_id_seq",allocationSize=1)
//    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="gen")
	public long userid=0;
	
	@Constraints.Required
	public String username;
	
	@Constraints.Required
    public String password;
	
	@Transient
	public String actor;
	
	@Transient
	public String pubSchema;
	
	@Transient
	public String lockYN;
	
	public String roleId ;
	
	public String role;
	
	

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public String getRoleId() {
		return roleId;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public String getRole() {
		return role;
	}

	public String getLockYN() {
		return lockYN;
	}

	public void setLockYN(String lockYN) {
		this.lockYN = lockYN;
	}


	public String getActiveYN() {
		return activeYN;
	}


	public void setActiveYN(String activeYN) {
		this.activeYN = activeYN;
	}


//	@Transient
	public String activeYN;
        
    
    public long getUserid() {
		return userid;
	}


	public void setUserid(long userid) {
		this.userid = userid;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	

	public String getActor() {
		return actor;
	}


	public void setActor(String actor) {
		this.actor = actor;
	}


	public String getPubSchema() {
		return pubSchema;
	}


	public void setPubSchema(String pubSchema) {
		this.pubSchema = pubSchema;
	}
//	public S
}
