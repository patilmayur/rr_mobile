package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class PersonJournalRoleInformationPK {
	
	/**
	 *Person identity number
	 */
	@Column(name="PR_PERSON_ID")
	private long prPersonId;

	/**
	 *Serial number
	 */
	@Column(name="SL_NO")
	private long serialNumber;
	
	/**
	 *Sets the value of Person identity number
	 *@param rrPrPersonId Person identity number
	 *@return void
	 */
	public void setPrPersonId(long rrPrPersonId)
	{
		prPersonId = rrPrPersonId;
	}

	/**
	 *Gets the Person identity number
	 *@return prPersonId
	 */
	public long getPrPersonId()
	{
		 return prPersonId;
	}

	/**
	 *Sets the value of Serial number
	 *@param rrSerialNumber Serial number
	 *@return void
	 */
	public void setSerialNumber(long rrSerialNumber)
	{
		serialNumber = rrSerialNumber;
	}

	/**
	 *Gets the Serial number
	 *@return serialNumber
	 */
	public long getSerialNumber()
	{
		 return serialNumber;
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(this == obj){
			return true;
		}
		if(obj instanceof PersonJournalRoleInformationPK)
		 {
			return true;
		  }
		
		return false;
	}

}
