package action;

import play.Logger;
import play.api.Configuration;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Action.Simple;
import play.mvc.Http.Context;
import play.mvc.Result;
import controllers.routes;


public class CacheAction extends Simple{

	@Override
	public Promise<Result> call(Context ctx) throws Throwable {
		Logger.info("calling action for " + ctx);
		
		if(ctx.session().get("username")==null || ctx.session().get("username").trim().length()==0){

//		ctx.request().
			
			
			Logger.info("calling action when session is clear " + ctx.session().get("username"));
			
			return F.Promise.pure(redirect(routes.UserController.loginPage())); 
		}else{
			
			ctx.response().setHeader("Cache-Control", "no-cache");
			ctx.response().setHeader("Cache-Control", "no-store");
			ctx.response().setHeader("Pragma", "no-cache");
			ctx.response().setHeader("Expires","0");
			
			Logger.info("calling action within session " + ctx.session().get("username"));
			
			return delegate.call(ctx);
		}
	}
}