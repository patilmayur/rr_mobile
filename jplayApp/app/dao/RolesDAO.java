package dao;

import java.util.Date;
import java.util.List;

import model.Roles;
import play.db.ebean.Model.Finder;

public class RolesDAO {

	
	 public static Finder<Long,Roles> find = new Finder<Long, Roles>(
		        Long.class, Roles.class
		      );
	 
	 public static List<Roles> viewRolesDetails(){
		 
		 return  find.all();
	 }
	 
	 public static Roles getRoleById(Long id) {
		        return find.where().eq("role_id", id).findUnique();
		    }
	 

	 public static void saveRole(Roles roles) {
		roles.setModifiedDate(new Date());
		roles.setModifiedBy(1);
		
		roles.save();
	 }

	public static void updateRole(Roles roles) {
		
		roles.setModifiedDate(new Date());
		roles.setModifiedBy(1);
		
		roles.update();
		
	}
	public static void deleteRole(Long id) {
        find.ref(id).delete();
    }
	
}
