package dao;

import java.util.List;
import java.util.Map;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;

import model.Publisher;
import play.db.ebean.Model.Finder;

public class PublisherDAO {

	public static Finder<Long,Publisher> find = new Finder<Long, Publisher>(
	        Long.class, Publisher.class
	      );
 
 public static List<Publisher> viewPublisherDetails(){
	 
	 return  find.all();
 }
 
 public static Publisher getPublisher(Long id) {
	        return find.where().eq("Publisher_id", id).findUnique();
 }
 
 @SuppressWarnings("unchecked")
public static Map<String,String> getPublisherData() {
     return (Map<String,String>) find.select("ACRONYM,FULL_NAME");
}
 
 @SuppressWarnings("unchecked")
 public static List<Publisher> getPublisherList() {
	 
	 EbeanServer server = Ebean.getServer("slack20_dba");
	 
	 List<Publisher>  list = (List<Publisher>) server.find(Publisher.class).select("databaseSchema,fullName")
    		  .orderBy("FULL_NAME")
    		  .findList();
      return list;
 }
 

 public static void saveData(Publisher pub) {
//	 Person.setModifiedDate(new Date());
//	 Person.setModifiedBy(1);
	
//	 person.save();
 }

public static void updateData(Publisher pub) {
	
//	roles.setModifiedDate(new Date());
//	roles.setModifiedBy(1);
	
//	person.update();
	
}
public static void deleteData(Long id) {
// find.ref(id).delete();
}
	
}
