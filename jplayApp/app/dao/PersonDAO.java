package dao;

import java.util.Date;
import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;

import model.Author;
import model.Person;
import model.User;
import play.Logger;
import play.db.ebean.Model.Finder;

public class PersonDAO {
	
	 public static Finder<Long,Person> find = new Finder<Long, Person>(
		        Long.class, Person.class
		      );
	 
	 public static List<Person> viewRolesDetails(){
		 
		 return  find.all();
	 }
	 
	 public static Person getPerson(Long id) {
		        return find.where().eq("pr_person_id", id).findUnique();
	 }
	 
	 public static Person authenticatePerson(User person, String serverName) {
		 Logger.debug("=**----serverName------*=*="+serverName);
		 
		 EbeanServer server = Ebean.getServer(serverName.toLowerCase());
		 
		Person persion =  server.find(Person.class).where()
		 .eq("USER_NAME", person.getUsername())
		 .eq("PASSWORD", person.getPassword())
		 .eq("ACTIVE_YN", "Y")
		 .eq("LOCK_YN", "N")
		 .findUnique();
	     
		 
		 return persion;
//		 find.where()
//	    		 .eq("USER_NAME", person.getUsername())
//	    		 .eq("PASSWORD", person.getPassword())
//	    		 .eq("ACTIVE_YN", "Y")
//	    		 .eq("LOCK_YN", "N")
//	    		 .findUnique();
	 }
	 
	 
	 public static void saveRole(Person person) {
//		 Person.setModifiedDate(new Date());
//		 Person.setModifiedBy(1);
		
//		 person.save();
	 }

	public static void updateRole(Person person) {
		
//		roles.setModifiedDate(new Date());
//		roles.setModifiedBy(1);
		
//		person.update();
		
	}
	public static void deletePerson(Long id) {  
//     find.ref(id).delete();
	}

}
