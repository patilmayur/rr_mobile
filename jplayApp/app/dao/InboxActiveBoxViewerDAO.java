package dao;

import java.util.List;

import model.InboxActiveBoxViewer;
import play.db.ebean.Model.Finder;

public class InboxActiveBoxViewerDAO {

	public static Finder<Long,InboxActiveBoxViewer> find = new Finder<Long, InboxActiveBoxViewer>(
	        Long.class, InboxActiveBoxViewer.class
	      );
 
 public static List<InboxActiveBoxViewer> viewRolesDetails(){
	 
	 return  find.all();
 }
 
 public static InboxActiveBoxViewer fetchById(Long id, String column) {
	        return find.where().eq(column, id).findUnique();
 }
 

 public static void save(InboxActiveBoxViewer iabv) {
//	 Person.setModifiedDate(new Date());
//	 Person.setModifiedBy(1);
	
//	 person.save();
 }

public static void update(InboxActiveBoxViewer iabv) {
	
//	roles.setModifiedDate(new Date());
//	roles.setModifiedBy(1);
	
//	person.update();
	
}
public static void delete(Long id) {  
//find.ref(id).delete();
}
	
}
