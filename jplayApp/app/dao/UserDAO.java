package dao;

import java.util.Date;
import java.util.List;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;

import model.User;
import play.db.ebean.Model.Finder;

public class UserDAO {
	
public static Finder<Long,User> find = new Finder<Long, User>(
	        Long.class, User.class
	      );
 
 public static List<User> viewUserDetails(String serverName){
	 
	 
	 return  find.all();
 }
 
 public static User getUserById(Long id) {
	        return find.where().eq("role_id", id).findUnique();
	    }
 
// public static User authenticateUser(User user) {
//     return find.where().eq("username", user.username).eq("password", user.password).findUnique();
// }

 public static void saveUserDetails(User User) {

//	 User.save();
 }

 public static void updateUserDetails(User User) {
	
//	User.update();
	
 }
 public static void deleteUser(Long id) {
//   	 find.ref(id).delete();
 }
 
}
