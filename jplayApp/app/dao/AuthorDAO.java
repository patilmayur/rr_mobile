package dao;

import java.util.List;

import model.Author;
import model.User;
import play.db.ebean.Model.Finder;

public class AuthorDAO {

	 public static Finder<Long,Author> find = new Finder<Long, Author>(
		        Long.class, Author.class
		      );
	 
	 public static List<Author> viewRolesDetails(){
		 
		 return  find.all();
	 }
	 
	 public static Author getAuthorById(Long id) {
		        return find.where().eq("au_author_id", id).findUnique();
	 }
	 

	 public static Author authenticateAuthor(User author) {
	     return find.where()
	    		 .eq("USER_NAME", author.getUsername())
	    		 .eq("PASSWORD", author.getPassword())
	    		 .eq("ACTIVE_YN", "Y")
	    		 .eq("LOCK_YN", "N")
	    		 .findUnique();
	 }
	 
	 public static void save(Author author) {
//		 Person.setModifiedDate(new Date());
//		 Person.setModifiedBy(1);
		
//		 person.save();
	 }

	public static void update(Author author) {
		
//		roles.setModifiedDate(new Date());
//		roles.setModifiedBy(1);
		
//		person.update();
		
	}
	public static void delete(Long id) {  
//  find.ref(id).delete();
	}
	
	
}
