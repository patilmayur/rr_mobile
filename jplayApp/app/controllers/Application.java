package controllers;

import java.io.File;
import java.util.*;

import org.apache.commons.io.FileUtils;

import model.Publisher;
import model.Roles;
import model.User;
import play.Logger;
import play.Play;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import views.html.createRole;
import views.html.index;
import views.html.roles;
import views.html.updateRole;
import action.CacheAction;
import dao.PublisherDAO;
import dao.RolesDAO;
import model.GetManuscriptDetails;



public class Application extends Controller {
	

    public static Result index() {
    	
    	if(session().get("schema")!=null){
    		session().remove("schema");
    	}
    	
    	Form<Publisher> pubForm = Form.form(Publisher.class);
    	  Map<String, String> publisherMap = new HashMap<String, String>();
    	  
//    	  publisherMap = PublisherDAO.getPublisherData();
    	  
    	  List<Publisher> pubList = PublisherDAO.getPublisherList();
    	  
    	 
         return ok(index.render(pubForm,pubList));
    }
    
  
      
    @With(CacheAction.class)
    public static Result viewRolesDetails(){
    	Form<Roles> rolesForm=Form.form(Roles.class);
    	
		return ok(roles.render(RolesDAO.viewRolesDetails(),rolesForm));
    }
    
    //Create New Role
    @With(CacheAction.class)
    public static Result createRole(){
    	Form<Roles> rolesForm=Form.form(Roles.class);
    	
		return ok(createRole.render(rolesForm));
    	
    }
    
//  Save Role
    @With(CacheAction.class)
    public static Result saveRole(){
    	Form<Roles> rolesForm=Form.form(Roles.class).bindFromRequest();
    	
    	
    	if(rolesForm.hasErrors()) {
    	    return badRequest(createRole.render(rolesForm));
    	  } else {
    		  RolesDAO.saveRole(rolesForm.get());
    		  return redirect(routes.Application.viewRolesDetails()); 
    	  }
    }
    
    //populate form fields with existing value
    @With(CacheAction.class)
    public static Result editRole(Long id){

        Roles role = RolesDAO.getRoleById(id);
        
        //fill() method is used to populate form fields with existing value
        Form<Roles> rolesForm = Form.form(Roles.class).fill(role);

        return ok(updateRole.render(rolesForm));
    }
    
    
//  Update Role
    @With(CacheAction.class)
    public static Result updateRole(){
    	
    	Form<Roles> rolesForm=Form.form(Roles.class).bindFromRequest();
    	
    	if(rolesForm.hasErrors()) {
    	    return badRequest(
    	    		updateRole.render(rolesForm)
    	    );
    	  } else {
    		  RolesDAO.updateRole(rolesForm.get());
    		  return redirect(routes.Application.viewRolesDetails()); 
    	  }
    	
    }
    
//  Delete Role
//    @With(CacheAction.class)
    public static Result deleteRole(Long id){
        Logger.debug("Delete Record Id Is : "+id);
        RolesDAO.deleteRole(id);

        return redirect(routes.Application.viewRolesDetails()); 
    }
	
}