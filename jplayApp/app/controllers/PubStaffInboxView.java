package controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import model.GetManuscriptDetails;
import model.User;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.staffInboxView;


public class PubStaffInboxView extends Controller{
	
	public static Result staffinboxView(){
		
		Map<String, List<GetManuscriptDetails>> staffInboxViewMap = new HashMap<String, List<GetManuscriptDetails>>();
		
		GetManuscriptDetails ms = new GetManuscriptDetails();
		
		User userDetails = new User();
		userDetails.setUserid(Long.parseLong((session().get("persionId"))));
		userDetails.setPubSchema(session().get("schema"));
		
  	     List<GetManuscriptDetails> msList = ms.getManudriptDetails(userDetails);
		
		 
//		 msList = ms.getManudriptDetails();
  	  
  	  
  	  
  	  Iterator<GetManuscriptDetails> itr = msList.iterator();
   	  while(itr.hasNext()){
   		  
   		GetManuscriptDetails gms = itr.next();
   		  
   		 Logger.debug("=**=*="+gms.getMs_no()+"=*=**="+gms.getFirst_name());
   	  }
   	  
   	  staffInboxViewMap.put("staffInboxViewList", msList);
   		  
//  	 Logger.debug("====================="+msList.size()+">>><<<<<<>>>"); 
//  	 Logger.debug("====================="+msList.get(1)+">>><<<<<<>>>"); 
//  	 Logger.debug("====================="+msList.get(0)+">>><<<<<<>>>"); 
  	 
//       return ok(index.render(pubForm,pubList));
		
		return ok(staffInboxView.render("",staffInboxViewMap));
	}

}
