package controllers;

//import org.junit.After;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.h2.engine.Role;

import action.CacheAction;
import model.Author;
import model.Person;
import model.User;
import play.Logger;
import play.Play;
import play.mvc.Http.MultipartFormData.FilePart;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Result;
import play.mvc.With;
import views.html.loginPage;
import views.html.signup;
import views.html.viewrole;
import dao.AuthorDAO;
import dao.PersonDAO;
import dao.UserDAO;
//import java.util.*;


public class UserController extends Controller {
	
	public static Result changeLocale(){
		DynamicForm requestData = Form.form().bindFromRequest();
	    String locale = requestData.get("locale");
	    changeLang(locale);
	    
//	    Map<String, String> options = new HashMap<String, String>();
//        options.put("locale", locale);
        
//        requestData.fill(options);
	    flash("locale",locale);
	    
		Form<User> userForm = Form.form(User.class);
		return ok(loginPage.render("", userForm));
		
	}

	public static Result loginPage() {
		
		DynamicForm requestData = Form.form().bindFromRequest();
	    String schema = requestData.get("publisher");
	    String actor = requestData.get("actor");
	    
	    session("actor",actor);
	    session("schema",schema);
	    
	     User user = new User();
	     user.setPubSchema(schema);
	    
//	    if(actor.equalsIgnoreCase("Author")){
//	    	
//	    }else{
//	    	
//	    }
		
	    Form<User> userForm = Form.form(User.class);		
		return ok(loginPage.render("", userForm));
	}
	
	public static Result checkin() {
		Form<User> userForm = Form.form(User.class).bindFromRequest();
		
//		Role.SCHEMA=
		if (userForm.hasErrors()) {
			
			return badRequest(loginPage.render("user.details", userForm));
		} else {
			
			if(session().get("actor").equalsIgnoreCase("Author")){
//				Logger.debug("=**session().get('Author')*=*="+session().get("actor"));
				Author author = AuthorDAO.authenticateAuthor(userForm.get());  
				
				if(author!=null){
					session().remove("username");
					session("username",author.getUserName());
					
					return redirect(routes.Application.viewRolesDetails());
				} else {
					
					return ok(loginPage.render("failed.login", userForm));
				}
				
			}	
				else{
					Logger.debug("=**session().get('schema')*=*="+session().get("schema"));
					
				Person person = PersonDAO.authenticatePerson(userForm.get(),session().get("schema"));	
				
				
				if(person!=null){
					session().remove("username");
					session().remove("persionId");
					session("username",person.getFirstName());
					session("persionId",""+person.getPersonId());
					
//					return redirect(routes.Application.viewRolesDetails());
					
					return redirect(routes.PubStaffInboxView.staffinboxView());
					
				} else {
					
					return ok(loginPage.render("failed.login", userForm));
				}
		    }
			
			  
//			User user = UserDAO.authenticateUser(userForm.get());
			
			/*if(user!=null){
				session().remove("username");
				session("username",user.username);
				
//				return ok(viewrole.render("Test"));
				return redirect(routes.Application.viewRolesDetails());
			} else {
				
				return ok(loginPage.render("failed.login", userForm));
			}*/
		}
	}        
 
	@With(CacheAction.class)
	public static Result logout(){
		
		 session().remove("username");
		 session().remove("persionId");
		 flash("success", "You have been logged out");
		 
//		 return redirect(routes.UserController.loginPage());
		 Form<User> userForm = Form.form(User.class);		
			return ok(loginPage.render("", userForm));
	}
	
	public static Result signup(){
		Form<User> userForm = Form.form(User.class);
		
		return ok(signup.render("create your account",userForm));
	}
	
	@With(CacheAction.class)
	public static Result addUser(){
		Form<User> userForm = Form.form(User.class).bindFromRequest();
		
    	
    	if(userForm.hasErrors()) {
    	    return badRequest(signup.render("",userForm)
    	    );
    	  } else {
    		  UserDAO.saveUserDetails(userForm.get());
    		  
    		  return ok(loginPage.render("", userForm)); 
    	  }
	}
	
	public static Result upload() throws IOException {
  	  
    	MultipartFormData body = request().body().asMultipartFormData();
    	 FilePart picture = body.getFile("picture");
    	  if (picture != null) {
    		  
    	    String fileName = picture.getFilename();
    	    File file = picture.getFile();
    	    
    	    String myUploadPath = Play.application().configuration().getString("myUploadPath"); 
    	    File newFile = new File(myUploadPath, "_"+fileName);
    	   
    	    //file.renameTo(newFile);
    	    
    	    FileUtils.copyFile(file, newFile);
    	    
    	    return ok("File Uploaded in " +newFile.getPath());
    	    
    	  } else {
    	    flash("error", "Missing file:");
    	    return redirect(routes.Application.viewRolesDetails());    
    	  }
    	}

}
