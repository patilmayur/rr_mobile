name := """jplayApp"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies += "commons-io" % "commons-io" % "2.3"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs
)


javaOptions ++= Seq("-Dconfig.file=conf/db_configuration.conf")
